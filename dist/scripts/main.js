'use strict';

// ready
$(document).ready(function () {

    // mask phone
    //$("[name=phone]").mask("+7 (999) 999-9999");
    // mask phone
    $('select[name="category"]').change(function () {
        var el = $(this).val();
        $('.search__inner .category').css('display', 'none');
        $('#category' + el).css('display', 'block');
    });

    $("input[name='category']").click(function () {
        var test = $(this).val();
        $(".desc").hide();
        $("#" + test).show();
        //console.log("#"+test)
    });

    // anchor
    $(".anchor").on("click", "a", function (event) {
        event.preventDefault();
        var id = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({ scrollTop: top + 40 }, 1000);
    });
    // anchor

    // slider
    $('.singleItem').slick({
        arrows: true,
        dots: false
    });
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: false,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        arrows: true,
        dots: false,
        centerMode: false,
        focusOnSelect: true,
        responsive: [{
            breakpoint: 767,
            settings: {
                arrows: false,
                dots: true,
                centerMode: true,
                centerPadding: '10px',
                slidesToShow: 2
            }
        }, {
            breakpoint: 479,
            settings: {
                arrows: false,
                dots: true,
                centerMode: true,
                centerPadding: '10px',
                slidesToShow: 1
            }
        }]
    });
    $('.slider-for2').slick({
        dots: true,
        infinite: true,
        arrows: false,
        speed: 500
        //fade: true
    });
    $('.slider-for2 .slick-dots li').on('mouseover', function () {
        $(this).parents('.slider-for2').slick('goTo', $(this).index());
    });
    //$('.slider-for1').slick({
    //    slidesToShow: 1,
    //    slidesToScroll: 1,
    //    arrows: false,
    //    fade: false,
    //    //asNavFor: '.slider-nav1'
    //    afterChange: function (slickSlider, i) {
    //        $('.slider-nav1 .slick-slide').removeClass('slick-active');
    //        $('.slider-nav1 .slick-slide').eq(i).addClass('slick-active');
    //    }
    //});
    //$('.slider-nav1 .slick-slide').eq(0).addClass('slick-active');
    //$('.slider-nav1').slick({
    //    slidesToShow: 4,
    //    slidesToScroll: 1,
    //    asNavFor: '.slider-for1',
    //    arrows: false,
    //    dots: true,
    //    centerMode: false,
    //    focusOnSelect: true
    //});
    //$('.slider-nav1').on('mouseenter', '.slick-slide', function (e) {
    //    var $currTarget = $(e.currentTarget),
    //        index = $currTarget.data('slick-index'),
    //        slickObj = $('.slider-for1').slick('getSlick');
    //    slickObj.slickGoTo(index);
    //
    //});

    // autocomplete
    var options = {
        data: [{ "name": "Адыгея" }, { "name": "Архангельская обл." }, { "name": "Астраханская обл." }, { "name": "Башкортостан" }, { "name": "Белгородская обл." }, { "name": "Брянская обл." }, { "name": "Владимирская обл." }, { "name": "Волгоградская обл." }, { "name": "Вологодская обл." }, { "name": "Воронежская обл." }, { "name": "Дагестан" }, { "name": "Ивановская обл." }, { "name": "Ингушетия" }, { "name": "Кабардино-Балкария" }, { "name": "Калининградская обл." }],
        getValue: "name",
        list: {
            match: {
                enabled: true
            }
        },
        template: {
            type: "custom",
            method: function method(value, item) {
                //return "<span class='flag flag-" + (item.img).toLowerCase() + "' ></span>" + value;
                return "<div class='row'>" + value + "</div>";
            }
        }
    };
    $("#search").easyAutocomplete(options);
    // autocomplete

    // select
    $('select:not(.multiple)').select2({
        minimumResultsForSearch: Infinity
    });
    //$('.multiple').multiselect({
    //    includeSelectAllOption: true
    //});
    $('.multiple').multipleSelect({
        placeholder: "Выберите характеристики",
        selectAll: false,
        minimumCountSelected: 2,
        selectAllText: 'Выделеть все',
        allSelected: 'Все выбрано',
        countSelected: '# из % выбрано',
        noMatchesFound: 'Не найдено совпадений'
    });
    // select

    // fancybox
    $('.imageLink').magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery: {
            enabled: true
        }
    });
    // fancybox

    $('.input__file-js').change(function () {
        $('.input__file-js').each(function () {
            var name = this.value;
            var reWin = /.*\\(.*)/;
            var fileTitle = name.replace(reWin, "$1");
            var reUnix = /.*\/(.*)/;
            fileTitle = fileTitle.replace(reUnix, "$1");
            //$(this).parent().parent().find('.input__name-js').val(fileTitle);
            //$(this).parent().find('.btn_js').text("Готово");
            //$(this).parent().find('.btn').text("Готово");
            $('.input__text-js').text('Загружено: ' + fileTitle);
        });
    });

    $('.category--js').each(function () {
        var countFil = $(this).find('.categoryBody--js li').length;
        //console.log(countFil);
        for (var i = 1; i < 10; i++) {
            $(this).css('z-index', i);
            //console.log(countFilNum);
        }
        if (countFil <= 5) {
            $(this).find('.categoryBtn--js').hide();
        }
    });
    $('.catalogBlock__item').each(function () {
        //var countFilNum = $(this).length;
    });

    $('.categoryBtn--js').click(function (e) {
        var countFil = $(this).parent().find('.categoryBody--js li').length;
        if (countFil > 5) {
            var $this = $(this).parent();
            var $thisText = $(this).find('span');
            var openedText = 'Свернуть категории';
            var closedText = 'Все категории';
            var itemHeight = 28;
            var defaultHeight = parseFloat(itemHeight * countFil);
            console.log(defaultHeight);
            var targetHeightBody = 134;
            var targetHeightInner = 157;
            //var targetHeight = 'none';
            var overflowWrap = $this.parent().find('.categoryBody--js');
            var animationTime = 500;
            e.preventDefault();
            $this.toggleClass('open');
            if ($this.hasClass('open')) {
                $thisText.text(openedText);
                $this.stop().animate({ 'height': defaultHeight + 21 }, animationTime);
                overflowWrap.stop().animate({ 'maxHeight': defaultHeight }, animationTime);
            } else {
                $thisText.text(closedText);
                $this.stop().animate({ 'height': targetHeightInner }, animationTime);
                overflowWrap.stop().animate({ 'maxHeight': targetHeightBody }, animationTime);
            }
        }
    });

    $('.search__js').click(function () {
        $(this).toggleClass('active').next().slideToggle();
    });
    $('.favor__js').click(function () {
        $(this).toggleClass('active').next().toggleClass('active').toggleClass('in');
    });
    $('.photo__close--js').click(function () {
        $(this).parent().hide();
    });

    $('.slider-line--js').each(function () {

        var per = $(this).attr('data-per');
        $(this).children().css('width', per + '%');
        if (per > 70) {
            $(this).children().css('background', '#52a529');
        } else if (per > 40 && per <= 70) {
            $(this).children().css('background', '#ff9f10');
        } else {
            $(this).children().css('background', '#e70c0c');
        }
    });

    //$('#popover').popover();
    $('.popover').mouseover(function () {
        var title = $(this).attr('data-title');
        var img = $(this).attr('data-img');
        var num = $(this).attr('data-num');
        var price = $(this).attr('data-price');
        $(this).next().html('<div class="popover-body--inner">' + '<div class="popover-body__img"><img src="' + img + '" alt="' + title + '"></div>' + '<div class="popover-body__desc"><p class="text--sm">' + num + '</p><p><b>' + title + '</b></p><p>' + price + '</p></div></div>');
    });
    $('.popover').mouseout(function () {
        $(this).next().html('');
    });
    function popover() {}
});
// ready

// load
$(document).load(function () {});
// load

// scroll
$(window).scroll(function () {});
// scroll

// mobile sctipts
var screen_width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
if (screen_width <= 767) {}
// mobile sctipts
//# sourceMappingURL=main.js.map